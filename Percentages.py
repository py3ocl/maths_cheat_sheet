class Percentages:

    @staticmethod
    def percentage_of(value, percent):
        return (value * percent) / 100

    @staticmethod
    def percentage_increase(start_value, end_value):
        return ((end_value - start_value) / abs(start_value)) * 100

    @staticmethod
    def percentage_decrease(start_value, end_value):
        return ((start_value - end_value) / abs(start_value)) * 100

    @staticmethod
    def percentage_difference(value1, value2):
        return ( abs(value1 - value2) / ((value1 + value2) / 2) ) * 100

if __name__ == "__main__":

    v = 40
    p = 10
    print(f"{p} percent of {v} is {Percentages.percentage_of(v, p)}")

    v1 = 4
    v2 = 8
    print(f"Percentage increase between {v1} and {v2} is {Percentages.percentage_increase(v1, v2)}")
    print(f"Percetnage decrease between {v2} and {v1} is {Percentages.percentage_decrease(v2, v1)}")
    print(f"Percentage difference between {v2} and {v1} is {Percentages.percentage_difference(v2, v1)}")
