class Maths:

    @staticmethod
    def within_tolerance(a, b, tolerance):
        return a >= b - tolerance and a <= b + tolerance

    # Find sqrt(n) using a binary search to find m * m == n, searching from a mid point.
    @staticmethod
    def sqrt(n, tolerance = 0.000000000000001):
        if n == 0:
            return 0
        lo = min(1, n)
        hi = max(1, n)
        mid = (lo + hi) / 2
        m2 = mid * mid

        while (not Maths.within_tolerance(m2, n, tolerance)):
            if (m2 > n):
                hi = mid
            else:
                lo = mid
            prev_mid = mid
            mid = (lo + hi) / 2
            if mid == prev_mid:
                break
            m2 = mid * mid

        return mid

    # Implementation of Euler's Number, or mathematical constant e.
    # Algoritm: 1 + (1/1) + (1/(1 * 2)) + (1/(1 * 2 * 3)) + ...
    @staticmethod
    def EulersNumber():
        prev_res = 1
        res = 0.0
        hi = 1
        while res != prev_res:
            frac = 1
            for n in range(1, hi):
                frac *= n
            prev_res = res
            res += 1.0 / frac
            hi += 1
        return res

if __name__ == "__main__":

    import math

    for n in [0, 1, 2, 10]:
        print(f" math.sqrt {n} = {math.sqrt(n)}")
        print(f"Maths.sqrt {n} = {Maths.sqrt(n)}")
