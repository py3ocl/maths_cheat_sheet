# Percentages

## Percentage Of

Calculate what is the percentage one value of another value:  
(V * P) / 100

Example:
```
4 is 10 percent of 40 or
4 is 10% of 40 or
10% of 40 is 4

(40 * 10) / 100 = 4
```

Python code example:
```
def percentage_of(value, percent):
    return (value * percent) / 100

print(percentage_of(40, 10))
```

## Percentage Increase

Calculating the percentage change between a start and end value:  
((V<sub>2</sub> - V<sub>1</sub>) / |V<sub>1</sub>|) * 100

Examples:
```
Percentage increase between 4 and 8 is 100%
Percentage increase between 250 and 1600 is 540%
```

Python code example:
```
def percentage_increase(start_value, end_value):
    return ((end_value - start_value) / abs(start_value)) * 100

print(percentage_increase(4, 8))
```

## Percentage Decrease

Calculating the percentage change between a start and end value:  
((V<sub>1</sub> - V<sub>2</sub>) / V<sub>1</sub>) * 100

Examples:
```
Percentage decrease between 8 and 4 is 50%
```

Python code example:
```
def percentage_decrease(start_value, end_value):
    return ((start_value - end_value) / abs(start_value)) * 100

print(percentage_decrease(8, 4))
```

## Percentage Difference

Percentage difference takes the mid point between two values, where neither value has a significant meaning, and uses the mid point to calculate a percentage.

Calculating the percentage of difference between two values:  
(|(V<sub>1</sub> - V<sub>2</sub>)| / ((V<sub>1</sub> + V<sub>2</sub>) / 2)) * 100

Example:
```
Percentage difference between 1 and 3 is 100%

(|(1 - 3)| / ((1 + 3) / 2)) * 100


Percentage difference between 4 and 8 is 66.6667%

(|(4 - 8)| / ((4 + 8) / 2)) * 100
```

Python code example:
```
def percentage_difference(value1, value2):
    return ( abs(value1 - value2) / ((value1 + value2) / 2) ) * 100

print(percentage_difference(8, 4))
```
