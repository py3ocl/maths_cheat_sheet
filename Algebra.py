import math

class Algebra:

    @staticmethod
    def exponent_of(x, y):
        return math.log(y) / math.log(x)

if __name__ == "__main__":

    x = 2
    y = 8
    print(f"Exponent of {x}\u207f = {y} is {Algebra.exponent_of(x,y)}")
