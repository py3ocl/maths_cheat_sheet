# Algebra

# Exponent Of

Calculate the exponent n where x<sup>n</sup> = y

n = log(y) / log(x)

Example:
```
The exponent n is 3 when x is 2 and y is 8.

log(8) / log(2) = 3
```

Python code example:
```
import math:

def exponent_of(x, y):
    return math.log(y) / math.log(x)

print(exponent_of(2, 8))
```
